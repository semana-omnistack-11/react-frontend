# React Front End

> ## A little description
> The presentation layer of this application. This was my first React app, and allows NGOs to register, and publish incidents which they need help with.

## Installing

```bash
$ git clone git@gitlab.com:semana-omnistack-11/react-frontend.git
$ yarn run install # You can use 'npm' if you prefer, too
```

## Run as Developer

```bash
$ yarn run start
$ yarn start
```

## Build for Production

```bash
$ yarn run build
$ yarn build
```

## Tests

Since there are not any implemented tests, I saw no point in talking about the test script

> # **Enjoy it ;)**
