import './styles.css'
import api from '../../services/api'
import React, { useState} from 'react'
import { FiLogIn } from 'react-icons/fi'
import logoImage from './../../assets/logo.svg'
import herosImage from './../../assets/heroes.png'
import { Link, useHistory } from 'react-router-dom'

// 974f94a12e
export default function Logon() {
  const [id, setId] = useState('')
  const history = useHistory()

  async function handleLogin(e) {
    e.preventDefault()
    
    try {
      const resp = await api.post('session', { ong_id: id })
      console.log(resp.data)

      localStorage.setItem('ong_id', id)
      localStorage.setItem('ong_name', resp.data.name)
      history.push('/profile')
    } catch (err) {
      console.error(err);
    }
  }

  return(
    <div className="logon-container">
      <section className="fom">
        <img src={ logoImage } alt="Heros"/>

        <form onSubmit={handleLogin}>
          <h1> Faça o seu login </h1>

          <input
            placeholder="Sua ID"
            value={id}
            onChange={e => setId(e.target.value)} />
          <button className="button" type="submit"> Entrar </button>

          <Link className="back-link" to="/register">
            <FiLogIn size={16} />
            Não tenho cadastro
          </Link>
        </form>


      </section>

      <img src={ herosImage } alt="Heros"/>
    </div>
  )
}