import './style.css'
import api from '../../services/api'
import React, {useState } from 'react'
import { FiArrowLeft } from 'react-icons/fi'
import logoImage from './../../assets/logo.svg'
import { Link, useHistory } from 'react-router-dom'


export default function NewCase() {
  const [title, set_title] = useState('')
  const [description, set_description] = useState('')
  const [value, set_value] = useState('')
  const history = useHistory()
  const ong_id = localStorage.getItem('ong_id')

  async function createCase() {
    try {
      await api.post('cases', {title, description, value}, {
        headers: {
          Authorization: ong_id
        }})
    } catch (err) {
      console.error(err)
    }
    history.push('/profile')
  }

  return(
    <div className="register-container">
      <div className="content">
        <section>
          <img src={logoImage} alt="Be The Hero"/>

          <h1> Cadastrar novo Caso </h1>
          <p> 
            Descreva o caso detalhadamente para encontrar um
            heroi praa salvar seu dia
          </p>

          <Link className="back-link" to="/profile">
            <FiArrowLeft size={16} />
            Voltar à Home
          </Link>
        </section>

        <form>
          <input 
            placeholder="Título do Caso"
            value={title}
            onChange={e => set_title(e.target.value)} />
          <textarea 
            placeholder="Descrição"
            value={description}
            onChange={e => set_description(e.target.value)} />
          <input 
            placeholder="Valor (R$)"
            value={value}
            onChange={e => set_value(e.target.value)} />

          <button className="button" type="submit" onClick={createCase} >
            Cadastrar
          </button>
        </form>
      </div>
    </div>
  )
}