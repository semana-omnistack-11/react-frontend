import './style.css'
import api from '../../services/api'
import logoImage from './../../assets/logo.svg'
import React, { useEffect, useState } from 'react'
import { FiPower, FiTrash2 } from 'react-icons/fi'
import { Link, useHistory } from 'react-router-dom'


export default function Profile() {
  const history = useHistory()
  const [cases, setCases] = useState([])
  const ong = localStorage.getItem('ong_name')
  const ong_id = localStorage.getItem('ong_id')

  useEffect(() => {
    api.get('profile', {
      headers: {
        Authorization: ong_id
      }
    }).then(resp => {
      setCases(resp.data)
    })
  }, [ong_id])

  async function deleteCase(id){
    try {
      await api.delete(`cases/${id}`, {
        headers: {
          Authorization: ong_id
        }
      })
      setCases(cases.filter(cc => cc.id !== id))
    } catch (error) {
      console.error(error)
    }
  }

  async function handleLogout() {
    localStorage.clear()
    history.push('/')
  }

  return(
    <div className="profile-container">
      <header>
        <img src={logoImage} alt="Be The Hero"/>
        <span> Bem Vindos, { ong } </span>

        <Link to="/cases/new" className="button">
          Cadastrar Novo Caso
        </Link>
        <button type="button" onClick={handleLogout}>
          <FiPower size={20} color="#E02041"/>
        </button>
      </header>

      <h1> Casos Registrados </h1>

      <ul>
        {cases.map(cc => (
        <li key={cc.id}>
          <strong>CASO</strong>
          <p> { cc.title } </p>

          <strong>DESCRIÇÃO</strong>
          <p> { cc.description } </p>
          
          <strong>VALOR</strong>
          <p> { Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL'}).format(cc.value) } </p>

          <button type="button" onClick={() => deleteCase(cc.id)}>
            <FiTrash2 size={20} color="#a8a8b3" />
          </button>
        </li>
        ))}
      </ul>
    </div>
  )
}