import React from 'react'
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import Logon from './pages/Logon'
import Profile from './pages/Profile'
import NewCase from './pages/NewCase'
import Register from './pages/Register'


export default function Routes() {
  return(
    <BrowserRouter>
      <Switch>
        <Route path='/' exact component={Logon} />
        <Route path='/register' component={Register} />
        <Route path='/profile' component={Profile} />
        <Route path='/cases/new' component={NewCase} />
      </Switch>
    </BrowserRouter>
  )
}